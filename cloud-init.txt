#!/bin/bash
# INPUT VARIABLES:
# <UDF name="USERNAME" Label="Secure User Name; not 'root'" />
# <UDF name="PASSWORD" Label="Secure User Password" />
# <UDF name="SSHKEY" Label="Secure User SSH Key" />
# <UDF name="SETUP_FIREWALL" Label="Firewall" oneOf="yes,no" default="yes" />
# <UDF name="SETUP_F2B" Label="Fail2ban with default configuration" oneOf="yes,no" default="yes" />
# <UDF name="TIMEZONE" Label="TZ Database Timezone" default="America/New_York" />
# <UDF name="HOST" Label="System Hostname" default="" />
# <UDF name="FQDN" Label="Fully Qualified Domain Name" default="" />
# enable logging
exec 1> >(tee -a "/var/log/new_linode.log") 2>&1
echo "=========================================================="
echo "=== Running Secure No Root Password Linode StackScript ==="
echo "=========================================================="

sudo apt-get install -y augeas-tools
augtool --autosave 'set /etc/ssh/sshd_config/AllowGroups ssh'

# Harden SSH Access
sed -i -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i -e 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
systemctl restart sshd
# Ignore root password lock and secure user setup if username is blank or root
if [ "$USERNAME" != "" ] && [ "$USERNAME" != "root" ]; then
    # Disable root password
    passwd --lock root
    # ensure sudo is installed and configure secure user
    apt-get -y install sudo
    adduser $USERNAME --disabled-password --gecos ""
    echo "$USERNAME:$PASSWORD" | chpasswd
    usermod -aG sudo,ssh,docker $USERNAME
    # configure ssh key for secure user
    SSHDIR="/home/$USERNAME/.ssh"
    mkdir $SSHDIR && echo "$SSHKEY" >> $SSHDIR/authorized_keys
    chmod -R 700 $SSHDIR && chmod 600 $SSHDIR/authorized_keys
    chown -R $USERNAME:$USERNAME $SSHDIR
fi
# Update system over IPv4 without any interaction
apt-get -o Acquire::ForceIPv4=true update
DEBIAN_FRONTEND=noninteractive \
  apt-get \
  -o Dpkg::Options::=--force-confold \
  -o Dpkg::Options::=--force-confdef \
  -y --allow-downgrades --allow-remove-essential --allow-change-held-packages
# Configure hostname and configure entry to /etc/hosts
IPADDR=`hostname -I | awk '{ print $1 }'`
echo -e "\n# The following was added via Linode StackScript" >> /etc/hosts
# Set FQDN and HOSTNAME if they aren't defined
if [ "$FQDN" == "" ]; then
    FQDN=`dnsdomainname -A | cut -d' ' -f1`
fi
if [ "$HOST" == "" ]; then
    HOSTNAME=`echo $FQDN | cut -d'.' -f1`
else
    HOSTNAME="$HOST"
fi
echo -e "$IPADDR\t$FQDN $HOSTNAME" >> /etc/hosts
hostnamectl set-hostname "$HOSTNAME"
# Configure timezone
timedatectl set-timezone "$TIMEZONE"
if [ "$SETUP_FIREWALL" == "yes" ]; then
    apt-get install ufw -y
    ufw default allow outgoing
    ufw default deny incoming
    # ufw allow 22,80,443,14567/udp,14690/udp,23000:23009/udp
    ufw allow 22
    ufw allow 80
    ufw allow 443
    ufw allow 14567/udp
    ufw allow 14690/udp
    ufw allow 23000:23009/udp
    ufw enable
fi
if [ "$SETUP_F2B" == "yes" ]; then
    apt install fail2ban -y
    cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
    cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
    systemctl start fail2ban
    systemctl enable fail2ban
fi
echo "======================================================================"
echo "=== Finished Installing Secure No Root Password Linode StackScript ==="
echo "======================================================================"

echo "==============================================="
echo "=== Running LinuxGSM setup ===================="
echo "==============================================="

sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install wget file tar bzip2 gzip unzip bsdmainutils python3 util-linux binutils bc jq tmux netcat lib32gcc-s1 lib32stdc++6 libncurses5:i386

curl -fsSL https://gitlab.com/BCButcher/server/-/blob/main/1942server.service -o /etc/systemd/system/1942server.service
systemctl daemon-reload
systemctl enable 1942server

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt update && sudo apt install -y nodejs
npm install gamedig -g

adduser bf1942server --disabled-password
su - bf1942server
wget -O linuxgsm.sh https://linuxgsm.sh && chmod +x linuxgsm.sh && bash linuxgsm.sh bf1942server
./bf1942server install
./bf1942server start

echo "================================================"
echo "=== Finished LinuxGSM setup ===================="
echo "================================================"
